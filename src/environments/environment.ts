export const environment = {
    production: false,
    enableDevToolRedux: true,
    openIdConnect: {
        enabled: true,
        stripIdTokenFromLocation: true,
        authorizeUrl: 'https://ecast.cc.cec.eu.int:7002/cas/oauth2/authorize',
        euLoginAccessTokenUrl: 'https://ecast.cc.cec.eu.int:7002/cas/oauth2/token',
        spaClientId: 'XLTF_bsc1IKfgxtqc58dkRF90_XXr9viMfB49nbLIBpZ-zu3WbVHt7BFZ5j3hhu48GEvMuHrEmAu4XW6tR_IPEr3yoGV0b5Wmm5SHDy4v-H3E8yZqHt_TeyIc49inzYg3SUfk4haSHHrt-st_5ZnS24TFAHtyRACtjzqosqdPFQ',
        spaRedirectUrl: 'http://d02di1637961dit.net1.cec.eu.int:4200/screen/home',
        apiGatewayAccessTokenUrl: 'https://10.136.85.44:9443/oauth2/token',
        apiGatewayConsumerKey: 'clc2c21IQk1mamZTckxhMHQ0a292R2p2eVYwYTpYMk9mbDJyenhJZUlzX3VHVVhUWlNPRjBDTGdh',
        apiGatewayServices: {
            exampleService: {
                endpoint: 'https://10.136.85.44:8243/test/0.1/dg',
                audienceId: 'tfRswrimPCLLswgEtVWvvrlfpwHkliEbJVFyUQMnwBo8BPlyjoxixXJLAjAdmEOcn0mXDDE1YpLnP3ASJ1x3krlhtvgtUU4JccpQ_NHMmeBK9WzJjqwknOUsVk5wvxgv6vHpT2paGjJcTLdF2lvLkFoG_NWbfJUMOPPKB6o5nGM'
            }
        },
        services: {
            exampleService: {
                endpoint: 'http://d02di1637950dit.net1.cec.eu.int:9010/dg',
                audienceId: 'tfRswrimPCLLswgEtVWvvrlfpwHkliEbJVFyUQMnwBo8BPlyjoxixXJLAjAdmEOcn0mXDDE1YpLnP3ASJ1x3krlhtvgtUU4JccpQ_NHMmeBK9WzJjqwknOUsVk5wvxgv6vHpT2paGjJcTLdF2lvLkFoG_NWbfJUMOPPKB6o5nGM'
            }
        }
    },
};

